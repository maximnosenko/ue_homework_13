// UE_Homework_13.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Helpers.h"
#include <random>
#include <ctime>

class Animal
{
public:
    virtual void Voice() {}
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof\n";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meoy\n";
    }
};

class Cow : public Animal
{
    void Voice() override
    {
        std::cout << "Muuu\n";
    }
};


int main()
{
    int ZooSize = 10;
    Animal** zoo = new Animal * [ZooSize];
    srand(time(NULL));
    for (int i = 0; i < ZooSize; i++)
    {
        switch (rand() % 3)
        {
        case 0:
            zoo[i] = new Dog();
            break;
        case 1:
            zoo[i] = new Cat();
            break;
        case 2:
            zoo[i] = new Cow();
            break;
        }
    }

    for (int i = 0; i < ZooSize; i++)
        zoo[i]->Voice();
}