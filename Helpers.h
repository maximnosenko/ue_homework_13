#pragma once

//Calculates and returns the square of sum of two numbers
int SquareSum(int a, int b)
{
	return (a + b) * (a + b);
}